# 1. Indholdsfortegnelse #
<!-- TOC -->

- [1. Indholdsfortegnelse](#1-indholdsfortegnelse)
- [2. Spørgsmål til Jacob, og noter](#2-spørgsmål-til-jacob-og-noter)
- [3. Dagbog](#3-dagbog)
    - [3.1. Formatet af dagbogen](#31-formatet-af-dagbogen)
    - [15.-16. maj](#15-16-maj)
    - [3.2. Mandag og tirsdag 8.-9. maj](#32-mandag-og-tirsdag-8-9-maj)
    - [3.3. Tirsdag d. 2. maj](#33-tirsdag-d-2-maj)
    - [3.4. Mandag d. 1. maj](#34-mandag-d-1-maj)
    - [3.5. Tirsdag d. 25. april](#35-tirsdag-d-25-april)
    - [3.6. Mandag d. 24. april](#36-mandag-d-24-april)
    - [3.7. Tirsdag d. 18. april](#37-tirsdag-d-18-april)
    - [3.8. Tirsdag d. 11. april](#38-tirsdag-d-11-april)
    - [3.9. Mandag d. 10. april](#39-mandag-d-10-april)
    - [3.10. Tirsdag d. 4. april](#310-tirsdag-d-4-april)
    - [3.11. Mandag d. 3. april](#311-mandag-d-3-april)

<!-- /TOC -->

# 2. Spørgsmål til Jacob, og noter # 

<!-- 
- Digital måde at bruge artefaktmodellen på - husk at beskriv det er en anderledes måde at bruge den på. 
- I aflevering 3, under teknisk udfordring, menes der f.eks. udfordringer med metoder. Her er det oplagt at snakke om vores måde at bruge artefaktmodellen på.
- I aflevering 3, brug scenarier til at beskrive før/efter og hold Jacob "i hånden", altså sæt ham ind i alt om projektet og konteksten (PACT etc.)
-->
- Vi har 5 minutters film; evt vise Jacob 30sekunder fra filmen til QA.

# 3. Dagbog #

## 3.1. Formatet af dagbogen ##
Dagbogen vil være opbygget så vi (oftest) gennemgår følgende fire punkter hver gang vi arbejder med projektet:
- Hvad lavede vi (hvad skulle vi nå)?
- Hvordan opnåede vi det?
- Opnåede vi det vi ville? Hvorfor?
- Evaluering: Hvad gik godt? Hvad gik skidt? Hvad kunne vi have gjort bedre?

## Onsdag 24. maj ##
Evaluerende møde om high-fidelity prototypen med bestyrelsesmedlemmet.

## Tirsdag 23. maj ##

Evaluerende møde om high-fidelity prototypen med studentermedhjælperen.

## 15.-16. maj ##

Her udviklede vi Hi-Fi prototypen i Angular.

## 3.2. Mandag og tirsdag 8.-9. maj ##

I denne uge havde vi fået evalueret papirsprototypen og de to-tre use cases med Amanda. Vi fik en masse gode insights, som vi kunne tage med videre når vi i denne uge skulle lave på hi-fidelity prototypen. Vi ville gerne nå meget af prototypen i denne uge; altså få lavet prototypen så den stemmer overens med de overordnede designtræk i papirsprototypen, med det input vi fik ud af evalueringen. 

Vi startede et Angular projekt, og begyndte at få sat hele UIen op, for senere at gå igang med at implementere use casen omkring indmelding af et medlem i SPBU. Vi fik også lavet login-funktionaliteten. Vores udfordringer lå egentligt kun i at ingen af os er Angular eksperter som sådan, altså er det lidt learning by doing. Til gengæld kan man relativt hurtigt få en interaktiv prototype til at køre.

Vi nåede det vi ville, og fik brugt insights fra evalueringen af papirsprototypen til at forbedre det nye design. Det kommer os til gode at vi har fået dokumenteret alle ting ved notering undervejs i processen, da vi altid kan finde tilbage til erfaringer m.m. fra tidligere.

Inden næste uge er omme skal vores prototype være fordige så vi kan få et sidste møde med studentermedhjælperen og bestyrelsesmedlemmet, for så at kunne skrive rapporten inden d. 26/5.

## 3.3. Tirsdag d. 2. maj ##

D. 2/5 havde vi vores papirprototype parat, og skulle mødes med Amanda (studentermedhjælperen) og bruge den til at få evalueret vores design. Altså var det i høj grad participatory design (Benyon). Vi ville gerne få opklaret fejl/mangler/misforståelser/... så vi kunne rette det til inden vi laver en hi-fidelity prototype.

Vi startede med at få introduceret konceptet omkring prototyping og participatory design som vi ville bruge det; vi havde 2-3 use cases vi ville have Amanda til at gennemgå i det nye system, og have hende til at tænke højt, spørge ind, forholde sig kritisk, osv.

Vi fik en masse gode insights, da Amanda var meget åben omkring sine tanker, og fik rettet os hvis vi havde bevæget os i en forkert retning eller manglede noget essentielt. 

Det fungerede enormt godt at følge de "practice" guidlines der er i Benyon omkring prototyping in practice. Vi kunne måske godt have forberedt flere typer af design til prototypen for at få forskellige perspektiver, men det vigtigste var at få funktionaliteten og use cases arbejdet igennem og se om de stemte overens med virkeligheden.

## 3.4. Mandag d. 1. maj ##

D. 1/5 fik vi færdiggjort vores papirprototype med den sidste use case, nemlig tilmelding af deltager til kursus i SPBU. Vi fik forberedt til dagen efter hvor Amanda skulle komme og vi skulle bruge prototypen i participatory design til at evaluere på designet. Vi fordelte roller; hvem tager noter, hvem er computeren i prototyping-processen, m.m.

Vi brugte use casen vi havde udviklet, til at designe prototypen ud fra; dermed sikrede vi at vi fik de relevante sider og skridt med i prototypen.

Det gik rigtig fint med at få lavet de forskellige visninger og interaktioner der skulle til i use casen.

## 3.5. Tirsdag d. 25. april ##

D. 25/4 evaluerede vi vores scenarier og læste dem igennem igen, før vi samlede dem i nogle use cases. Formålet er at vi ender ud med nogle brugbare use cases, hvor vi har nok viden til at lave papirsprototype ud fra et par af disse. 

Vi fik diskuteret undervejs hvad vi forestillede os og læst fra noterne fra de interviews vi har haft, og fik beskrevet use cases i skridt, så vi kunne lave papirsprototype for nogle af dem. Undervejs i skabelsen af papirsprototypen fik vi diskuteret hvordan tingene skulle se ud og hvad vi skulle have med. Det endte med at vi kun fik lavet prototype for én use case, nemlig indmelding af medlemmer i systemet. Her har vi så lavet flere views, og sørget for at når vi skal evaluere denne med studentermedhjælperen og få hende til at gennemgå opgaven i det "nye" system (på papiret godt nok), at det er interaktivt.

Vi opnåede ikke at lave prototype for to use cases som vi gerne ville have, men vi fik lavet en god prototype hvor vi tror på at vi kan få en masse gode inputs fra kommende evaluering med studentermedhjælperen. Vi kunne godt have planlagt dette lidt tidligere, så vi ikke var så sent ude, men vi kommer efter det. Selve prototypearbejdet gik godt, og vi skal have den lavet færdig 1/5 og så holde evaluering med studentmedhjælperen 2/5.

## 3.6. Mandag d. 24. april ##

D. 24/4 lavede vi scenarier som skulle udgøre forarbejdet til vores use cases. Vi lavede 2-3 scenarier til de emner det gav mening, og til de emner hvor vi allerede havde nok viden til at lave use cases, lavede vi ikke scenarier. Vi prøvede at dække de forskellige måder at bruge en funktionalitet på, som f.eks. indmeldelse af medlem. Her kan man komme ud for at medlemmet er dårlig betaler og måske ikke bør meldes ind, eller er et gammelt medlem så man kan bruge de informationer man har på medlemmet fra tidligere.

Vi brugte Benyon til at guide os i skabelsen af scenarierne, og fik beskrevet dem rimelig konkret, så vi kunne samle dem til use cases næste gang. Altså opnåede vi det primære formål med dagen, og regner med at nå papirsprototypen d. 25/4.

## 3.7. Tirsdag d. 18. april ##

<!-- Opsamling på interview #1 med bestyrelsesmedlem, samt udvidelse af PACT-analyse efter den opnåede viden af dette interview -->

D. 18/4 startede vi ud med opsamling på sidste interview vi havde (første interview med Helle fra bestyrelsen). Herefter lavede vi en artefaktmodel. Formålet med opsamlingen var at få samlet de vigtigste insights og evalueret lidt på interviewet og resultaterne fra det. Formålet med artefaktmodellen var at få overblik over hvilke artefakter der var i brug, og deres begrænsninger/problemer, og formål.

Til opsamling fra interview noterede vi de vigtigste ting vi snakkede om i interviewet - senere vil vi se om vi kan inddrage UED, men ifølge bogen skal vi først have styr på f.eks. storyboards/use cases. Dette var meget simpelt, men skabte overblik over hvad vi fik snakket om i interviewet. 

Da vi arbejdede med artefaktmodellen, var vi nødt til at samle en liste af artefakter (jvf. collection-fasen i Benyon 12.6), da vi ikke gjorde det i vores kontekstuelle interview. Da vi havde indsamlet billeder af artefakterne, lavede vi en model og beskrev ud fra s. 286 i Benyon hvad det indeholder, hvilke problemer det kan bringe, kommunikation, breakdowns, etc.

Vi fik fint output af vores arbejde med artefaktmodellen, og fik dannet overblik over breakdowns, formål med artefakterne, hvor mange der var, og hvad de indeholdte. Dog var det mindre godt at vi opdagede så sent at vi skulle have indsamlet/billeder af artefakter i det kontekstuelle interview med Amanda - dette måtte vi så gøre denne dag hvor vi skulle have haft dem allerede. Det gjorde processen lidt tungere end den havde været ellers.

## 3.8. Tirsdag d. 11. april ##

<!-- Interview med bestyrelsesmedlemmet Helle fra KBH; kravsspecificering, forventningsafstemning m.m. -->

D. 11/4 holdte vi interview med vores kontakt i bestyrelsen for SPBU, Helle. Vi mødtes i gruppen en time inden FaceTime-interviewet skulle finde sted, for at tjekke efter om alt var klar inden vi skulle starte. Da klokken blev 18 og vi skulle ringe, viste det sig at det var det forkerte nummer, så vi blev nødt til at sende en mail med vores nummer så Helle kunne ringe på dét.

Da vi fik forbindelse, introducerede vi Helle til faget, hvem vi var, samt det forventede output af hele projektet, nemlig en prototype og en del insigter i hvad der kan gøres bedre, og vores vinkel hvordan det kan gøres. Vi sørgede for at få tilladelse til at optage interviewet, og høre om Helles rolle i bestyrelsen ("kasseansvarlig"). 

Herefter spurgte vi ind til bestyrelsens brug af det nuværende 'system', kommunikation både i bestyrelsen og mellem f.eks. Helle og studentermedhjælperen Amanda, m.m.

Til sidst evaluerede vi kort og sikrede at der ikke var noget vi manglede at få opklaret, samt at Helle ikke havde mere hun synes vi skulle ind på.

Ud af alt dette fik vi gennem et semistruktureret interview og diskussion en masse insigter som vi ikke kunne få gennem stud.medhj. Amanda, da det er et helt andet perspektiv. 
Vi opnåede altså det vi gerne ville (høre krav, forventninger, idéer, ...), også selvom starten var lidt uheldig. En anden gang ved vi nu at det er vigtigt at aftale helt konkret HVEM der ringer til HVILKET nummer, så vi ikke havner i samme situation. Til gengæld fungerede det rigtig godt med strukturen af interviewet, og det føltes som om at Helle også var tilpas ved flowet undervejs.

## 3.9. Mandag d. 10. april ##

<!-- Interview forberedelse; introduktion, spørgsmål, vinkler, etc. -->

D. 10/4 forberedte vi interviewet med bestyrelsesmedlemmet Helle, som er vores kontakt i bestyrelsen i forløbet. 

Første gjorde vi optagelsessoftware klar på Nikolaj's computer, så vi kunne lydoptage interviewet med Helle, som skulle foregå på FaceTime. Herefter brugte vi bogens beskrivelse (Benyon 7.3) til at få struktureret og gjort klar til interviewet. Vi aftalte roller; Nikolaj som interviewer, Rasmus og Teis som notetagere og opfølgere på uklarheder m.m. Vi gjorde en introduktion klar som skulle forberede Helle på emnerne, forklare hvem vi var og hvad formålet med projektet var, så vi var på samme side. Herefter forberedte vi 12 faste spørgsmål, men med plads til opfølgende spørgsmål og løs snak. 

Forberedelsen gik fint, vi fik struktureret og forberedt interviewet så vi havde noget at komme med på dagen. Det fungerede godt at bruge Benyon som værktøj til at læse om gode praktikker omkring interviews. 

## 3.10. Tirsdag d. 4. april ##

<!-- Møde med studentermedhjælper (Amanda). -->
D. 4/4 holdte vi kontekstuelt interview med studentermedhjælperen Amanda. Formålet med dette interview var primært at få overblik over de processer der indgår i arbejdet, hvilke krav vi kunne udlede til et nyt system, og på den måde få en bedre understanding af konteksten. Vi ville gerne se hvilke ting der var problematiske i måden arbejdet foregår på nu, gennem det arbejde stud.medhj. skulle udføre.

Vi holdte et kontekstuelt interview, hvor stud.medhj. gennemgik nogle typiske (og vigtige) arbejdsgange, så vi kunne observere detaljerne omkring dem. Undervejs spurgte vi ind og diskuterede for at fremme udbyttet. Vi fandt mangler/problemer undervejs, men lærte også mere om de krav der ville være til et kommende system. Herefter lavede vi en opsamling for at få overblik over de mange insights der kom ud af interviewet.

Vi fik i høj grad et udbytte af processen - både krav, arbejdsgange, problemer m.m. blev dækket i mere eller mindre grad. Vi fik nedskrevet undervejs så vi ikke mistede insights (forglemsomhed eller andet) efter interviewet.

Vi fik efter interviewet også adgang til SPBU's stud.medhj.-vejledningsdokument. Dette indeholder alle arbejdsgange beskrevet formelt, hvilket er en enorm hjælp for os som designere. Derudover har vi det udbytte vi fik af interviewet, som også har et andet perspektiv end dokumentet (formelt perspektiv), nemlig  stud.medhj.-perspektivet. Vi bør nu have nok information tilgængeligt til at vi om en uge (10/4) kan udarbejde en PACT-analyse, samt artefakt-model.

<!-- Evaluer interviewproces - hvad kunne vi gøre bedre -->


## 3.11. Mandag d. 3. april ##

Vi ville gerne have opstillet en overordnet plan for projektets forløb og de processer der skal indgå. Derudover ville vi også estimere opgaver og skabe en backlog. Til sidst ville vi gerne forberede os på det første (kontekstuelle) interview med studentermedhjælperen. Planlægning af første møde med både bestyrelsesmedlem og studentermedhjælper fandt sted.

Vi brugte [Trello](https:\\www.trello.com) til at få opstillet en backlog, et sprint-afsnit, m.m., til at skabe strukturen vi ville have. Her kunne vi også tilføje estimeringer af opgavers kompleksitet målt i timer. Vi fik alle vores opgaver skrevet i backloggen, og tilføjet de opgaver vi vil nå i denne uge til vores nuværende "sprint". Herefter læste vi i Benyons 12.2 afsnit om kontekstuelt interview, og forberedte os på første interview der skal laves i morgen 4/4.

Vi fik skabt struktur og overblik, og føler os klar til interviewet i morgen 4/4. Hjælpeværktøjet Trello var med til at skabe struktur, og gjorde det lettere for os at på overblikket.

Vi fik en bedre forståelse af vores projekt og hvad vi vil nå med det. Vi fik planlagt møde med studentermedhjælper til dagen efter. Vi fik også planlagt FaceTime-møde med bestyrelsesmedlem til 11/4.