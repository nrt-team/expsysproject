import { SPBUPage } from './app.po';

describe('spbu App', () => {
  let page: SPBUPage;

  beforeEach(() => {
    page = new SPBUPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
