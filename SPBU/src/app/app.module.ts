import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { OverviewComponent } from './overview/overview.component';
import { NavigationComponent } from './navigation/navigation.component';
import { RegisterComponent } from './register/register.component';
import { CoursesComponent } from './courses/courses.component';
import { UnregisterComponent } from './unregister/unregister.component';
import { Ng2TableModule } from 'ng2-table';
import { PaginationModule } from 'ng2-bootstrap';
import { AuthService } from './auth.service';
import { MemberService } from './member.service';
import { DatePipe } from '@angular/common';
import { PopoverModule } from 'ngx-popover';
import { TypeaheadModule } from 'ngx-bootstrap/typeahead';
import { CourseService } from './course.service';
import { CourseDetailComponent } from './course-detail/course-detail.component';
import { MessageService } from './message.service';
import { AlertModule } from 'ng2-bootstrap/alert';
import { CourseRegistrationComponent } from './course-registration/course-registration.component';

const appRoutes: Routes = [
  {path: 'login', component: LoginComponent},
  {path: 'overview', component: OverviewComponent},
  {path: 'courses', component: CoursesComponent},
  {path: 'courses/:id', component: CourseDetailComponent},
  {path: 'courses/:id/register', component: CourseRegistrationComponent},
  {path: 'register', component: RegisterComponent},
  {path: 'unregister', component: UnregisterComponent},
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    OverviewComponent,
    NavigationComponent,
    RegisterComponent,
    CoursesComponent,
    UnregisterComponent,
    CourseDetailComponent,
    CourseRegistrationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ReactiveFormsModule,
    Ng2TableModule,
    PopoverModule,
    AlertModule.forRoot(),
    TypeaheadModule.forRoot(),
    PaginationModule.forRoot(),
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    MessageService,
    AuthService,
    MemberService,
    DatePipe,
    CourseService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
