import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MemberService } from '../member.service';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Member } from '../models/member';
import { CourseService } from '../course.service';
import { Course } from '../models/course';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-course-registration',
  templateUrl: './course-registration.component.html',
  styleUrls: ['./course-registration.component.css']
})
export class CourseRegistrationComponent implements OnInit {

  registeredMembers: Member[];
  course: Course;

  registerForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    name: new FormControl("", [Validators.required]),
    workplace: new FormControl("", []),
    paymentDetails: new FormControl("", [Validators.required]),
    spbu: new FormControl("", []),
    dp: new FormControl("", [])
  });

  constructor(private memberService: MemberService,
              private courseService: CourseService,
              private router: Router,
              private route: ActivatedRoute,
              private messageService: MessageService) {
    this.course = this.courseService.getCourse(+this.route.snapshot.params.id);
  }

  ngOnInit() {
    this.registeredMembers = this.memberService.getMembers();
  }

  typeaheadOnSelect($event) {
    this.registerForm.patchValue($event.item);
    this.registerForm.controls.spbu.disable();
  }

  addParticipant() {
    let data = this.registerForm.getRawValue();
    data.paymentStatus = "Afventer";

    this.courseService.addParticipant(this.course.id, data);
    if (data.spbu) {
      this.messageService.postMessage({
        type: 'success',
        msg: 'Tilføjede medlem ' + data.spbu + ' til kurset ' + this.course.name,
        timeout: 5000
      });
    } else {
      this.messageService.postMessage({
        type: 'success',
        msg: 'Tilføjede ekstern deltager ' + data.name + ' til kurset ' + this.course.name,
        timeout: 5000
      });
    }


    this.router.navigate(['courses', this.route.snapshot.params.id]);
  }

}
