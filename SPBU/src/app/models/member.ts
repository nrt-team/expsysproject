export class Member {
  public name: string;
  public email: string;
  public address: string;
  public spbu: string;
  public dp: string;
  public active: boolean;
  public paymentStatus: PaymentStatus;
  public registerDate: Date;
  public unregisterDate: Date;
  public courseList: number[];

  constructor(name: string, email: string, address: string, dp: string, spbu: string) {
    this.name = name;
    this.email = email;
    this.address = address;
    this.dp = dp;
    this.courseList = [];
    this.spbu = spbu;
    this.active = true;
    this.registerDate = new Date();
    this.paymentStatus = PaymentStatus.Afventer;
  }
}

export enum PaymentStatus {
  Betalt,
  Afventer,
  Mangler
}
