export class Course {
  public id: number;
  public name: string;
  public participants: any[];
  public date: Date;
  public standbyList: string[];
  public paymentDue: Date;
  public cancelDue: Date;
  public description: string;
  public location: string;

  constructor(name: string, date: Date, standbyList: string[], paymentDue: Date,
              cancelDue: Date, description: string, location: string, id: number) {
    this.name = name;
    this.participants = [];
    this.date = date;
    this.standbyList = standbyList;
    this.paymentDue = paymentDue;
    this.cancelDue = cancelDue;
    this.description = description;
    this.location = location;
    this.id = id;
  }
}
