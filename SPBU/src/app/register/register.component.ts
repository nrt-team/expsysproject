import { Component, OnInit } from '@angular/core';
import { MemberService } from '../member.service';
import { Router } from '@angular/router';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Member } from '../models/member';
import { MessageService } from '../message.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  registeredMembers: Member[];
  previousRegistered = false;

  registerForm = new FormGroup({
    email: new FormControl("", [Validators.required, Validators.email]),
    name: new FormControl("", [Validators.required]),
    address: new FormControl("", [Validators.required]),
    spbu: new FormControl("", [Validators.required]),
    dp: new FormControl("", [])
  });

  constructor(private memberService: MemberService,
              private router: Router,
              private messageService: MessageService) {
  }

  ngOnInit() {
    this.registerForm.patchValue({
      spbu: this.memberService.generateIdentifier()
    });

    this.registeredMembers = this.memberService.getMembers();
  }

  typeaheadOnSelect($event) {
    this.previousRegistered = true;
    this.registerForm.patchValue($event.item);
  }

  registerMember() {
    let data = this.registerForm.value;

    if (!this.previousRegistered) {
      this.memberService.newMember(data.name, data.email, data.address, data.dp, data.spbu);
      this.messageService.postMessage({
        type: 'success',
        msg: 'Medlem ' + data.spbu + ' oprettet',
        timeout: 5000
      });
    } else {
      data.active = true;
      this.memberService.updateMember(data);
      this.messageService.postMessage({
        type: 'success',
        msg: 'Opdaterede medlem ' + data.spbu,
        timeout: 5000
      });
    }

    this.router.navigate(['overview']);
  }
}
