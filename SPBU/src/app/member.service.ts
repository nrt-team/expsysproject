import { Injectable } from '@angular/core';
import { Member, PaymentStatus } from './models/member';

@Injectable()
export class MemberService {

  private identifierCount: number = 25;
  private members: Member[] = [];

  constructor() {
    this.members = memberArray;
  }

  public newMember(name: string, email: string, address: string, dp: string, spbu: string): Member {
    let member = new Member(name, email, address, dp, spbu);
    this.members.push(member);
    return member;
  }

  public updateMember(data: any) {
    let member = this.members.find(member => member.spbu === data.spbu);
    Object.assign(member, data);
  }

  public getMember(spbu: string): Member {
    return Object.assign({}, this.members.find(member => member.spbu === spbu));
  }

  public getMembers(): Member[] {
    let memberArray = [];
    for (let i = 0; i < this.members.length; i++) {
      memberArray.push(Object.assign({}, this.members[i]));
    }
    return memberArray;
  }

  public generateIdentifier(): string {
    this.identifierCount++;
    return "SPBU-" + this.identifierCount;
  }
}

export const memberArray: Member[] =
  [
    {
      "name": "Nikolaj Quorning",
      "email": "nikolaj.quorning@gmail.com",
      "address": "Thorvaldsensgade 31, 4. 3. 8000 Århus C",
      "spbu": "SPBU-1",
      "dp": "DP-501",
      "active": false,
      "paymentStatus": PaymentStatus.Betalt,
      "registerDate": new Date("2016-01-16"),
      "unregisterDate": new Date("2017-05-17"),
      "courseList": []
    },
    {
      "name": "Rasmus Plauborg",
      "email": "rplau@gmail.com",
      "address": "Lollandsgade 4, 8000 Århus C",
      "spbu": "SPBU-2",
      "dp": "DP-502",
      "active": true,
      "paymentStatus": PaymentStatus.Mangler,
      "registerDate": new Date("2016-11-10"),
      "unregisterDate": new Date("2015-10-02"),
      "courseList": []
    },
    {
      "name": "Teis Petersen",
      "email": "teis.n.p@eclipsent.com",
      "address": "Finlandsgade 53",
      "spbu": "SPBU-3",
      "dp": "DP-503",
      "active": false,
      "paymentStatus": PaymentStatus.Betalt,
      "registerDate": new Date("2015-09-01"),
      "unregisterDate": new Date("2017-01-15"),
      "courseList": []
    },
    {
      "name": "Amanda Maj Bruun",
      "email": "amanda.bruun@hotmail.com",
      "address": "Thorvaldsensgade 31, 4. 3. 8000 Århus C",
      "spbu": "SPBU-4",
      "dp": "DP-504",
      "active": false,
      "paymentStatus": PaymentStatus.Betalt,
      "registerDate": new Date("2015-09-01"),
      "unregisterDate": new Date("2017-01-15"),
      "courseList": []
    }
  ];
