import { Component, OnInit } from "@angular/core";
import { MemberService } from "../member.service";
import { DatePipe } from "@angular/common";

@Component({
  selector: 'app-overview',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css']
})
export class OverviewComponent implements OnInit {

  public rows: Array<any> = [];
  public columns: Array<any> = [
    {
      title: 'Navn',
      name: 'name',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'E-mail',
      name: 'email',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Adresse',
      name: 'address',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'SPBU-nr.',
      name: 'spbu',
      sort: '',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'DP-nr.',
      name: 'dp',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Aktiv',
      name: 'active',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Betalingsstatus',
      name: 'paymentStatus',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Registreringsdato',
      name: 'registerDate',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public length: number = 0;
  public totalMissing: number = 0;
  public totalPending: number = 0;
  public totalPayed: number = 0;

  public config: any = {
    paging: true,
    sorting: {
      columns: this.columns
    },
    filtering: {
      filterString: ''
    },
    className: [
      'ng-table',
      'table-striped',
      'table-bordered',
      'table-hover'
    ]
  };

  private data: Array<any> = [];

  public constructor(private memberService: MemberService,
                     private datePipe: DatePipe) {
    this.length = this.data.length;
  }

  public ngOnInit(): void {
    this.data = this.memberService.getMembers();

    // Handle data formatting, and counting for view
    this.data.forEach(row => {
      row.active = row.active ? "Aktiv" : "Inaktiv";
      row.registerDate = this.datePipe.transform(row.registerDate, 'yyyy-MM-dd');

      switch (row.paymentStatus) {
        case 0:
          row.paymentStatus = "Betalt";
          this.totalPayed++;
          break;
        case 1:
          row.paymentStatus = "Afventer";
          this.totalPending++;
          break;
        case 2:
          row.paymentStatus = "Mangler";
          this.totalMissing++;
          break;
      }
    });
    this.onChangeTable(this.config);
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = {page: this.page, itemsPerPage: this.itemsPerPage}): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    console.log(data);
  }
}
