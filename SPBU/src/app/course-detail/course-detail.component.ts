import { Component, OnInit } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { Course } from "../models/course";
import { CourseService } from "../course.service";
import { MemberService } from "../member.service";

@Component({
  selector: 'app-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {

  public rows: Array<any> = [];
  public columns: Array<any> = [
    {
      title: 'Navn',
      name: 'name',
      sort: 'asc',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'E-mail',
      name: 'email',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Arbejdsplads',
      name: 'workplace',
      sort: '',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'SPBU-nr.',
      name: 'spbu',
      sort: '',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'DP-nr.',
      name: 'dp',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Betalingsstatus',
      name: 'paymentStatus',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: {
      columns: this.columns
    },
    filtering: {
      filterString: ''
    },
    className: [
      'ng-table',
      'table-striped',
      'table-bordered',
      'table-hover'
    ]
  };

  private data: Array<any> = [];
  private course: Course;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private courseService: CourseService) {
    this.course = courseService.getCourse(+route.snapshot.params.id);
  }

  ngOnInit() {
    this.data = this.course.participants;
    this.data.forEach(row => {
      switch (row.paymentStatus) {
        case 0:
          row.paymentStatus = "Betalt";
          break;
        case 1:
          row.paymentStatus = "Afventer";
          break;
        case 2:
          row.paymentStatus = "Mangler";
          break;
      }
    });
    this.data.forEach(member => member.workplace ? null : member.workplace = "");
    this.onChangeTable(this.config);
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = {page: this.page, itemsPerPage: this.itemsPerPage}): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  onAddParticipantClick() {
    this.router.navigate(['courses', this.course.id, 'register']);
  }

}
