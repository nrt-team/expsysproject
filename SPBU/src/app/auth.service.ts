import { Injectable } from "@angular/core";

@Injectable()
export class AuthService {

  currentUser: string;

  getCurrentUser() {
    return this.currentUser;
  }

  authUser(username: string, password: string): boolean {
    let valid = false;

    users.forEach((u) => {
      if (u.username === username && u.password === password) {
        this.currentUser = username;
        valid = true;
      }
    });

    return valid;
  }

  logout() {
    this.currentUser = undefined;
  }
}

const users = [
  {
    username: "amb",
    password: "amb"
  }
];
