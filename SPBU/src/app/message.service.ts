import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

export interface Message {
  type: string;
  msg: string;
  timeout: number;
}

export class SuccessfulMessage implements Message {
  type: string = 'success';
  msg: string;
  timeout: number;

  constructor(msg: string, timeout?: number) {
    this.msg = msg;
    this.timeout = timeout || 2000;
  }
}

@Injectable()
export class MessageService {

  public messages: Observable<Message[]>;
  private _messages: BehaviorSubject<Message[]>;

  constructor() {
    this._messages = <BehaviorSubject<Message[]>>new BehaviorSubject([]);
    this.messages = this._messages.asObservable();
  }

  postMessage(msg: Message): void {
    this._messages.next([msg]);
  }

  postMessages(msg: Message[]): void {
    this._messages.next(msg);
  }
}
