import { Component, OnInit } from "@angular/core";
import { CourseService } from "../course.service";
import { DatePipe } from "@angular/common";
import { Router } from "@angular/router";

@Component({
  selector: 'app-courses',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  public rows: Array<any> = [];
  public columns: Array<any> = [
    {
      title: 'Kursusnavn',
      name: 'name',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Dato',
      name: 'date',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Betalingsfrist',
      name: 'paymentDue',
      sort: 'asc',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Status',
      name: 'status',
      sort: 'asc',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    },
    {
      title: 'Deltagere',
      name: 'participants',
      filtering: {
        filterString: '',
        placeholder: 'Søg...'
      }
    }
  ];

  public page: number = 1;
  public itemsPerPage: number = 10;
  public maxSize: number = 5;
  public length: number = 0;

  public config: any = {
    paging: true,
    sorting: {
      columns: this.columns
    },
    filtering: {
      filterString: ''
    },
    className: [
      'ng-table',
      'table-striped',
      'table-bordered',
      'table-hover'
    ]
  };

  private data: Array<any> = [];

  public constructor(private courseService: CourseService,
                     private router: Router,
                     private datePipe: DatePipe) {
    this.length = this.data.length;
  }

  ngOnInit() {
    this.data = this.courseService.getCourses();
    this.data.forEach(row => {
      row.status = (new Date() > row.date) ? "Afholdt" : "Kommende";
      let dateFormat = 'yyyy-MM-dd';
      row.cancelDue = this.datePipe.transform(row.cancelDue, dateFormat);
      row.paymentDue = this.datePipe.transform(row.paymentDue, dateFormat);
      row.date = this.datePipe.transform(row.date, dateFormat);
      row.participants = "" + row.participants.length;
    });
    this.onChangeTable(this.config);
  }

  public changePage(page: any, data: Array<any> = this.data): Array<any> {
    let start = (page.page - 1) * page.itemsPerPage;
    let end = page.itemsPerPage > -1 ? (start + page.itemsPerPage) : data.length;
    return data.slice(start, end);
  }

  public changeSort(data: any, config: any): any {
    if (!config.sorting) {
      return data;
    }

    let columns = this.config.sorting.columns || [];
    let columnName: string = void 0;
    let sort: string = void 0;

    for (let i = 0; i < columns.length; i++) {
      if (columns[i].sort !== '' && columns[i].sort !== false) {
        columnName = columns[i].name;
        sort = columns[i].sort;
      }
    }

    if (!columnName) {
      return data;
    }

    // simple sorting
    return data.sort((previous: any, current: any) => {
      if (previous[columnName] > current[columnName]) {
        return sort === 'desc' ? -1 : 1;
      } else if (previous[columnName] < current[columnName]) {
        return sort === 'asc' ? -1 : 1;
      }
      return 0;
    });
  }

  public changeFilter(data: any, config: any): any {
    let filteredData: Array<any> = data;
    this.columns.forEach((column: any) => {
      if (column.filtering) {
        filteredData = filteredData.filter((item: any) => {
          return item[column.name].match(column.filtering.filterString);
        });
      }
    });

    if (!config.filtering) {
      return filteredData;
    }

    if (config.filtering.columnName) {
      return filteredData.filter((item: any) =>
        item[config.filtering.columnName].match(this.config.filtering.filterString));
    }

    let tempArray: Array<any> = [];
    filteredData.forEach((item: any) => {
      let flag = false;
      this.columns.forEach((column: any) => {
        if (item[column.name].toString().match(this.config.filtering.filterString)) {
          flag = true;
        }
      });
      if (flag) {
        tempArray.push(item);
      }
    });
    filteredData = tempArray;

    return filteredData;
  }

  public onChangeTable(config: any, page: any = {page: this.page, itemsPerPage: this.itemsPerPage}): any {
    if (config.filtering) {
      Object.assign(this.config.filtering, config.filtering);
    }

    if (config.sorting) {
      Object.assign(this.config.sorting, config.sorting);
    }

    let filteredData = this.changeFilter(this.data, this.config);
    let sortedData = this.changeSort(filteredData, this.config);
    this.rows = page && config.paging ? this.changePage(page, sortedData) : sortedData;
    this.length = sortedData.length;
  }

  public onCellClick(data: any): any {
    this.router.navigate(['courses', data.row.id]);
  }
}

