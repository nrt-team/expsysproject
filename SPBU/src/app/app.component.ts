import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { MessageService, Message } from './message.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']

})
export class AppComponent implements OnInit {

  alerts: Observable<Message[]>;

  constructor(private messageService: MessageService) {
  }

  ngOnInit() {
    this.alerts = this.messageService.messages;
  }
}
