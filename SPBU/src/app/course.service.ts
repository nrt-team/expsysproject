import { Injectable } from '@angular/core';
import { Course } from './models/course';
import { memberArray } from './member.service';

@Injectable()
export class CourseService {

  private courses: Course[];

  constructor() {
    this.courses = courses;
  }

  public getCourse(id: number): Course {
    return Object.assign({}, this.courses.find(course => course.id === id));
  }

  public getCourses(): Course[] {
    let courseArray = [];
    for (let i = 0; i < this.courses.length; i++) {
      courseArray.push(Object.assign({}, this.courses[i]));
    }
    return courseArray;
  }

  public addParticipant(courseId: number, data: any) {
    let course = this.getCourse(courseId);
    course.participants.push(data);
  }
}


const courses = [
  {
    "id": 0,
    "name": "Personlighedsforstyrrelser hos unge: Diagnostik, udredning og behandling",
    "participants": memberArray,
    "date": new Date("2017-08-24"),
    "standbyList": [],
    "paymentDue": new Date("2017-02-24"),
    "cancelDue": new Date("2017-01-13"),
    "description": "v./ Sune Bo, PhD og specialist i psykoterapi. Sune Bo arbejder klinisk i uddannelsesstilling til specialpsykolog ved børne- og ungdomspsykiatrien i Region Sjælland og er tilknyttet den psykiatriske forskningsenhed som seniorforsker. Sune Bo forsker i personlighedsforstyrrelser hos unge, bl.a. i form af et RCT-studie ift. effekten af MBT-gruppebehandling ved borderline personlighedsforstyrrelse hos unge.",
    "location": "Aarhus"
  },
  {
    "id": 1,
    "name": "Spædbarnets emotionelle og relationelle udvikling og spædbarnsobservation",
    "participants": memberArray.slice(0,2),
    "date": new Date("2017-03-13"),
    "standbyList": [],
    "paymentDue": new Date("2017-01-05"),
    "cancelDue": new Date("2017-01-05"),
    "description": "v./Cand.psych. Anne Holländer, specialist og supervisor i klinisk børnepsykologi og psykoterapi, privatpraktiserende. - Cand.psych. Heidi Rose, specialist og supervisor i klinisk børnepsykologi og psykoterapi, privatpraktiserende. - Cand.psych. Annette Anbert, specialpsykolog i børne- og ungdomspsykiatri, specialist og supervisor i klinisk børnepsykologi og psykoterapi, psykologfaglig ledende koordinator, Børne- og ungdomspsykiatrisk afdeling, region Sjælland.",
    "location": "Aalborg"
  },
  {
    "id": 2,
    "name": "Gruppeterapi for unge",
    "participants": [],
    "date": new Date("2017-02-08"),
    "standbyList": [],
    "paymentDue": new Date("2017-03-30"),
    "cancelDue": new Date("2017-02-16"),
    "description": "v./ Birte Lausch, cand.psych., specialist og supervisor i klinisk børnepsykologi og psykoterapi, diplomeret gruppeanalytiker,  souschef/chefpsykolog på Holmstrupgård; Gunvor Brandt, cand.psych.,specialist og supervisor i klinisk børnepsykologi og psykoterapi, diplomeret gruppeanalytiker. Specialpsykolog i børne- og ungdomspsykiatri. Ledende psykolog, Børne- og Ungdomspsykiatrisk Center, Region Midt.",
    "location": "Aarhus"
  },
  {
    "id": 3,
    "name": "Biologiske behandlingsmetoder II",
    "participants": [],
    "date": new Date("2017-01-09"),
    "standbyList": [],
    "paymentDue": new Date("2017-04-15"),
    "cancelDue": new Date("2017-04-09"),
    "description": "v./ Katrine Pagsberg, speciallæge i Børne- og ungdomspsykiatri, PhD, overlæge, Glostrup; Niels Bilenberg, professor i børne- og ungdomspsykiatri, OUH; Gesche Jürgens, afdelingslæge, Klinisk Farmakologisk afdeling, RH; Thorsten Schumann, speciallæge i Børne- og ungdomspsykiatri, overlæge, Augustenborg.",
    "location": "Aarhus"
  }
];

