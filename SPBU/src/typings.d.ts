/* SystemJS module definition */
declare var module: NodeModule;
interface NodeModule {
  id: string;
}

/* Allow JSON file imports */
declare module "*.json" {
  const value: any;
  export default value;
}
